

var colorArray = new Array("#ffb6b6","#fde2e2","#aacfcf","#EB4559","#F78259","#844685", "#EF962D",
    "#D7385E","#FB7B6B","#E7D39F","#856C8B","#827397","#00A8CC");
var textArray = new Array("A","B","c","D","E","F","G","H","I","J");
var textArray2 = [...textArray];
var index=9;
var div = document.getElementById("game");
let scene,card,cardFront,cardBack,he;
for(let idx=0; idx<20;idx++){
    scene = document.createElement("div");
    scene.setAttribute("class","scene");
    card = document.createElement("div");
    card.setAttribute("class","card");
    cardFront = document.createElement("div");
    cardFront.setAttribute("class","card-face cardFront");
    const cardId = "cardFront"+idx;
    cardFront.setAttribute("id", cardId);
    cardBack = document.createElement("div");
    cardBack.setAttribute("class","card-face cardBack");
    he = document.createElement("h2");
    const h2Id = "h"+idx;
    he.setAttribute("id",h2Id);


    //const index = Math.floor(Math.random() * index);
    var Text = document.createTextNode(textArray2[index]);
    textArray2.splice(index,1);
    index--;
    if(index<0) {
    index=9;
    textArray2=[...textArray];
    }
    he.appendChild(Text);
    cardBack.appendChild(he);
    card.appendChild(cardFront);
    card.appendChild(cardBack);
    scene.appendChild(card);
    div.appendChild(scene);
}

function setColorRandomly(){
    const parent = document.getElementById("game");
    const cards = parent.querySelectorAll("div.cardFront");
    for(let idx=0;idx<cards.length;idx++){
        const index = Math.floor(Math.random() * 13);
        const color = colorArray[index];
        const id = "cardFront"+idx;
        document.getElementById(id).style.backgroundColor = color;

    }
}
document.addEventListener("DOMContentLoaded", function(event) { 
    
    let secondFlip = false;
    let firstCard, secondCard;
   
    function flipTheCard() {
        if (this === firstCard) return;
        this.classList.add('flip-Card');
        if (!secondFlip) {
            secondFlip = true;
            firstCard = this;
            return;
        }
        secondCard = this;
        matchTheCards();

        function matchTheCards() {

            var child = firstCard.querySelector("h2");
            var text =  document.getElementById(child.id);
            child = secondCard.querySelector("h2");
            var text2 =  document.getElementById(child.id);
            if (text.innerHTML === text2.innerHTML) {
                turnOffFlip();
            }
            else{
            unFlipCards();
            }
        }
        
        function turnOffFlip() {
            firstCard.removeEventListener('click', flipTheCard);
            secondCard.removeEventListener('click', flipTheCard);
            secondFlip = false;
            firstCard = null;
            secondCard = null;
        }
        
        function unFlipCards() {
            setTimeout(() => {
                firstCard.classList.remove('flip-Card');
                secondCard.classList.remove('flip-Card');
                secondFlip = false;
                firstCard = null;
                secondCard = null;
            }, 1000);
        }
    }
    const card = document.querySelectorAll('.card');
    card.forEach(card => card.addEventListener('click', flipTheCard));
});




